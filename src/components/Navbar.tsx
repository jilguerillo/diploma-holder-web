import { NavLink } from 'react-router-dom';
import './Navbar.css';

const Navbar = () => {
    return (
        <nav className="navbar">

            <div className="navbar-logo">
                <NavLink to="/">Diploma Holder</NavLink>
            </div>
            <div className="navbar-links">
                <NavLink to="/" className="nav-link" >
                    Home
                </NavLink>
                <NavLink to="/profiles" className="nav-link">
                    Profiles
                </NavLink>
                <NavLink to="/about" className="nav-link">
                    About
                </NavLink>
            </div>
        </nav>
    );
};

export default Navbar;
