import diploma_extension_data_science from "../../assets/images/2023/diploma_extension_data_science.png";
import diploma_tecnicas_java from "../../assets/images/2019/diploma_tecnicas_java.jpg";
import diploma_rust from "../../assets/images/2022/diploma-rust.jpg";

export const ages = [2019, 2020, 2021, 2022, 2023, 2024] as const;

export const IMAGES: Record<typeof ages[number], Record<string, string>> = {
    2019: {
        diploma_tecnicas_java: diploma_tecnicas_java
    },
    2020: {},
    2021: {},
    2022: {
        diploma_rust: diploma_rust
    },
    2023: {
        diploma_extension_data_science: diploma_extension_data_science,
    },
    2024: {},
};