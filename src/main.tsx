import React from 'react'
import ReactDOM from 'react-dom/client'
import App from './App.tsx'
import { createBrowserRouter, RouterProvider } from "react-router-dom"
import './index.css'
import NotFoundPage from './pages/error/NotFoundPage.tsx'
import ProfilesPage from './pages/ProfilesPage.tsx'
import ProfilePage from './pages/ProfilePage.tsx'
import { Holder } from './pages/Holder.tsx'

/*
const router = createBrowserRouter([
  {
    path: '/',
    element: <App />,
    errorElement: <NotFoundPage />
  },
  {
    path: '/profiles',
    element: <ProfilesPage />,
    children: [
      {
        path: ':profileId',
        element: <ProfilePage />,
        children: [
          {
            path: ':imageId',
            element: <Holder />
          }
        ]
      }
    ]
  }
]);
*/

const router = createBrowserRouter([
  {
    path: '/',
    element: <App />,
    errorElement: <NotFoundPage />
  },
  {
    path: '/profiles',
    element: <ProfilesPage />,
    children: [
      {
        path: ':profileId',
        element: <ProfilePage />,
      },
      {
        path: ':profileId/:imageId',
        element: <Holder />
      } 
    ]
  }
]);

ReactDOM.createRoot(document.getElementById('root')!).render(
  <React.StrictMode>
    <RouterProvider router={router}/>
  </React.StrictMode>,
)
