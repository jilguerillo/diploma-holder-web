import { Link, useParams } from "react-router-dom";
import { IMAGES, ages } from "../utils/constants";
import './Holder.css';  // Asegúrate de tener los estilos en Holder.css

type Year = 2019 | 2020 | 2021 | 2022 | 2023 | 2024;

export const Holder = () => {
    const { profileId, imageId } = useParams<{ profileId: string; imageId: string }>();

    const profileYear = profileId ? parseInt(profileId) : undefined;
    const isValidYear = (year: number): year is Year => ages.includes(year as Year);
    
    const imagesForYear = profileYear && isValidYear(profileYear) ? IMAGES[profileYear] : {};
    const imageSrc = imagesForYear[imageId!];

    return (
        <>
            <section className="holder">
                <div>
                    {imageSrc ? (
                        <img src={imageSrc} alt={imageId} className="image" />
                    ) : (
                        <p className="not-found">Image not found</p>
                    )}
                </div>
                <div>
                    <Link to='/'>Come Back</Link>
                </div>
            </section>
        </>
    );
};

export default Holder;
