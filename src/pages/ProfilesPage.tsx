import { NavLink, Outlet } from 'react-router-dom';
import { ages } from '../utils/constants';
import './ProfilesPage.css';

export default function ProfilesPage() {
  return (
    <section className="profiles-page">
      <div className="links-container">
        {ages.map((age) => (
          <NavLink
            key={age}
            to={`/profiles/${age}`}
            className={({ isActive }) => (isActive ? 'active-link' : 'nav-link')}
          >
            {age}
          </NavLink>
        ))}
      </div>
      <Outlet />
    </section>
  );
}
