import { NavLink, useParams } from "react-router-dom";
import { ages, IMAGES } from "../utils/constants";
import './ProfilePage.css';  // Asegúrate de tener los estilos en ProfilePage.css

type Year = 2019 | 2020 | 2021 | 2022 | 2023 | 2024;

export default function ProfilePage() {
    const { profileId } = useParams<{ profileId: string }>();
    const profileYear = profileId ? parseInt(profileId) : undefined;
    const isValidYear = (year: number): year is Year => ages.includes(year as Year);

    const imagesForYear = profileYear && isValidYear(profileYear) ? IMAGES[profileYear] : {};

    return (
        <section className="profile-page">
            <div className="links-container">
                {Object.keys(imagesForYear).map((imageKey) => (
                    <NavLink
                        key={imageKey}
                        to={`/profiles/${profileId}/${imageKey}`}
                        className="nav-link"
                    >
                        {imageKey}
                    </NavLink>
                ))}
            </div>
        </section>
    );
}
