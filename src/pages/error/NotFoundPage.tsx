import { Link } from "react-router-dom";
import './NotFoundPage.css'

export default function NotFoundPage() {
    return (
      <div className="flex flex-col gap-2">404 Not Found Page
        <Link to="/">Come back</Link>
      </div>
    )
}
